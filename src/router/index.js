import Vue from 'vue'
import VueRouter from 'vue-router'
import authGuard from '@/utils/authGuard'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('@/layouts/default.vue'),
    beforeEnter: authGuard,
    children: [
      {
        path: '',
        name: 'HomePage',
        component: () => import('@/views/frontOffice/preAuth/home.vue')
      },
      {
        path: 'garbage',
        name: 'GarbagePage',
        component: () => import('@/views/frontOffice/preAuth/garbage.vue')
      },
      {
        path: 'ranking',
        name: 'RankingPage',
        component: () => import('@/views/frontOffice/preAuth/ranking.vue')
      },
      {
        path: 'contact',
        name: 'ContactPage',
        component: () => import('@/views/frontOffice/preAuth/contact.vue')
      },
      {
        path: 'feedback',
        name: 'FeedbackPreAuthPage',
        component: () => import('@/views/frontOffice/preAuth/feedback.vue')
      }
    ]
  },
  {
    path: '/front-office',
    component: () => import('@/layouts/frontOffice.vue'),
    beforeEnter: authGuard,
    children: [
      {
        path: 'transaction-history',
        name: 'TransactionHistoryPage',
        component: () => import('@/views/frontOffice/postAuth/history.vue')
      },
      {
        path: 'feedback',
        name: 'FeedbackPostAuthPage',
        component: () => import('@/views/frontOffice/preAuth/feedback.vue')
      }
    ]
  },
  {
    path: '/back-office',
    component: () => import('@/layouts/blank.vue'),
    children: [
      {
        path: 'auth',
        name: 'AuthBackOfficePage',
        component: () => import('@/views/backOffice/auth')
      }
    ]
  },
  {
    path: '/back-office',
    component: () => import('@/layouts/backOffice.vue'),
    beforeEnter: authGuard,
    children: [
      {
        path: 'dashboard',
        name: 'DashboardPage',
        component: () => import('@/views/backOffice/dashboard.vue')
      },
      {
        path: 'manage-garbage-deposits',
        name: 'ManageGarbageDepositsPage',
        component: () => import('@/views/backOffice/manage/garbageDeposits.vue')
      },
      {
        path: 'manage-news',
        name: 'ManageNewsPage',
        component: () => import('@/views/backOffice/manage/news.vue')
      },
      {
        path: 'manage-garbage',
        name: 'ManageGarbagePage',
        component: () => import('@/views/backOffice/manage/garbage.vue')
      },
      {
        path: 'manage-category',
        name: 'ManageCategoryPage',
        component: () => import('@/views/backOffice/manage/category.vue')
      },
      {
        path: 'manage-feedback',
        name: 'ManageFeedbackPage',
        component: () => import('@/views/backOffice/manage/feedback.vue')
      }
    ]
  },
  {
    path: '*',
    name: 'NotFound',
    component: () => import('@/views/notfound.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
