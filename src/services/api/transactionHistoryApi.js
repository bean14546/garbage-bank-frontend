import instance from '@/services'

export default {
  find: () => {
    return instance.get('transactionHistory').then(response => response.data)
  },
  findById: (id) => {
    return instance.get(`transactionHistory/${id}`).then(response => response.data)
  },
  create: (data) => {
    return instance.post('transactionHistory', data).then(response => response.data)
  },
  update: (id, data) => {
    return instance.put(`transactionHistory/${id}`, data).then(response => response.data)
  },
  delete: (id) => {
    return instance.delete(`transactionHistory/${id}`).then(response => response.data)
  }
}
